const { v4: uuidv4 } = require('uuid');

module.exports = function uuidHandler(req, res) {
  if (req.method === 'GET' && req.url === '/uuid') {
    const uuid = uuidv4();
    const jsonResponse = { uuid: uuid };

    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify(jsonResponse, null, 2));
  } else {
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('Not Found');
  }
};
