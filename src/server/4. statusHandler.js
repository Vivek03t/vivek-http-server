module.exports = function statusHandler(req, res) {
  const urlParts = req.url.split('/');
  const statusCode = Number(urlParts.pop());

  if (
    req.method === 'GET' &&
    urlParts.length === 2 &&
    urlParts[1] === 'status' &&
    !isNaN(statusCode)
  ) {
    res.writeHead(statusCode, { 'Content-Type': 'text/plain' });
    res.end(`Status code: ${statusCode}`);
  } else {
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('Not Found');
  }
};
