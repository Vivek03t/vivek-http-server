module.exports = function delayHandler(req, res) {
  const urlParts = req.url.split('/');
  const delayInSeconds = Number(urlParts.pop());

  if (
    req.method === 'GET' &&
    urlParts.length === 2 &&
    urlParts[1] === 'delay' &&
    !isNaN(delayInSeconds)
  ) {
    setTimeout(() => {
      res.writeHead(200, { 'Content-Type': 'text/plain' });
      res.end('Delayed response');
    }, delayInSeconds * 1000);
  } else {
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('Not Found');
  }
};
