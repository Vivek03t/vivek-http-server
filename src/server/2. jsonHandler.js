const jsonData = {
  slideshow: {
    author: 'Yours Truly',
    date: 'date of publication',
    slides: [
      {
        title: 'Wake up to WonderWidgets!',
        type: 'all',
      },
      {
        items: [
          'Why <em>WonderWidgets</em> are great',
          'Who <em>buys</em> WonderWidgets',
        ],
        title: 'Overview',
        type: 'all',
      },
    ],
    title: 'Sample Slide Show',
  },
};

module.exports = function jsonHandler(req, res) {
  if (req.method === 'GET' && req.url === '/json') {
    res.writeHead(200, { 'Content-type': 'application.json' });
    res.end(JSON.stringify(jsonData, null, 2));
  } else {
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('Not Found');
  }
};
