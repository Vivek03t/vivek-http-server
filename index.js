const http = require('http');
const url = require('url');

const htmlHandler = require('./src/server/1. htmlHandler');
const jsonHandler = require('./src/server/2. jsonHandler');
const uuidHandler = require('./src/server/3. uuidHandler');
const statusHandler = require('./src/server/4. statusHandler');
const delayHandler = require('./src/server/5. delayHandler');

const server = http.createServer((req, res) => {
  const parsedUrl = url.parse(req.url, true);
  const pathname = parsedUrl.pathname;

  if (pathname === '/html' && req.method === 'GET') {
    htmlHandler(req, res);
  } else if (pathname === '/json' && req.method === 'GET') {
    jsonHandler(req, res);
  } else if (pathname === '/uuid' && req.method === 'GET') {
    uuidHandler(req, res);
  } else if (pathname.startsWith('/status/') && req.method === 'GET') {
    statusHandler(req, res);
  } else if (pathname.startsWith('/delay/') && req.method === 'GET') {
    delayHandler(req, res);
  } else {
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('Not Found');
  }
});

const port = process.env.PORT || 3000;
server.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
